﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace LauncherV2.app
{
    public partial class Program : System.Windows.Application
    {
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            this.StartupUri = new System.Uri("MainWindow.xaml", System.UriKind.Relative);
        }

        private void InitEmbeddedDllLoading()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                String name = new System.Reflection.AssemblyName(args.Name).Name;
                //name = name.Replace(".resources", "");
                String resourceName = "LauncherV2.AdminTool.Resources." +
                    name + ".dll";

                using (var stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {
                    Byte[] assemblyData = new Byte[stream.Length];

                    stream.Read(assemblyData, 0, assemblyData.Length);

                    return System.Reflection.Assembly.Load(assemblyData);

                }

            };
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            // hook on error before app really starts
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            base.OnStartup(e);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // TODO: Put Code here to email/upload info on crash through web service
            MessageBox.Show(e.ExceptionObject.ToString());
        }

        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public static void Main()
        {
            Program app = new Program();
            app.InitEmbeddedDllLoading();
            app.InitializeComponent();
            app.Run();
        }
    }
}
