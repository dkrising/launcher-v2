﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using LauncherV2.Core.Repair;
using System.Windows.Forms;
using LauncherV2.Core.Concurrency;
using LauncherV2.Core;
using System.Threading;

namespace LauncherV2.AdminTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private string repairDirectory;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        #region REPAIRER
        private void TabRepair_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            txtBaseUrl.Text = "Base URL: "+Settings.Default.RepairUrl;
        }

        private void btnCreateIndex_Click(object sender, RoutedEventArgs e)
        {
            btnCreateIndex.IsEnabled = false;
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                repairDirectory = dlg.SelectedPath;
                Repairer.SetProgressBarControl(pbRepair);
                Repairer.SetProgressTextControl(txtRepairProgress);
                Thread repair = new Thread(new ThreadStart(RepairWorker));
                repair.Name = "Repair";
                repair.IsBackground = true;
                repair.Start();
            }
            else
            {
                btnCreateIndex.IsEnabled = false;
            }
        }

        private void RepairWorker()
        {
            
            Repairer.CreateIndex(repairDirectory, new Action(() =>
            {
                ThreadSafe.SetControlPropertyThreadSafe(btnCreateIndex, "IsEnabled", true);
            }));
        }
        #endregion

        

        
    }
}
