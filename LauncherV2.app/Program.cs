﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;

namespace LauncherV2.app
{
    public partial class App : Application
    {

        private static string _settings = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<configuration>\n\t<configSections>\n\t<sectionGroup name=\"userSettings\" type=\"System.Configuration.UserSettingsGroup, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\" >\n\t<section name=\"LauncherV2.Core.Settings\" type=\"System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\" allowExeDefinition=\"MachineToLocalUser\" requirePermission=\"false\" />\n\t<section name=\"LauncherV2.core.Settings\" type=\"System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\" allowExeDefinition=\"MachineToLocalUser\" requirePermission=\"false\" />\n    </sectionGroup>\n  </configSections>\n  <userSettings>\n    <LauncherV2.Core.Settings>\n\t<setting name=\"MinSegmentSize\" serializeAs=\"String\">\n\t<value>200000</value>\n      </setting>\n      <setting name=\"MinSegmentLeftToStartNewSegment\" serializeAs=\"String\">\n\t<value>30</value>\n      </setting>\n      <setting name=\"RetryDelay\" serializeAs=\"String\">\n\t<value>5</value>\n      </setting>\n      <setting name=\"MaxRetries\" serializeAs=\"String\">\n\t<value>10</value>\n      </setting>\n      <setting name=\"MaxSegments\" serializeAs=\"String\">\n\t<value>5</value>\n      </setting>\n      <setting name=\"DownloadFolder\" serializeAs=\"String\">\n\t<value>./patch/</value>\n      </setting>\n      <setting name=\"VoteLink\" serializeAs=\"String\">\n\t<value>http://www.xtremetop100.com/in.php?site=1132306386</value>\n      </setting>\n      <setting name=\"LastVoteTime\" serializeAs=\"String\">\n\t<value />\n      </setting>\n      <setting name=\"NewsUrl\" serializeAs=\"String\">\n\t<value>http://dekaronuprising.net/update9ob2/news.json</value>\n      </setting>\n      <setting name=\"LauncherVersion\" serializeAs=\"String\">\n\t<value>0.1</value>\n      </setting>\n      <setting name=\"CurrentPatch\" serializeAs=\"String\">\n\t<value>0.1</value>\n      </setting>\n      <setting name=\"PatchUrl\" serializeAs=\"String\">\n\t<value>http://dekaronuprising.net/update9ob2/</value>\n      </setting>\n      <setting name=\"TimeServer\" serializeAs=\"String\">\n\t<value>time-a.nist.gov</value>\n      </setting>\n      <setting name=\"UpdateSystemTime\" serializeAs=\"String\">\n\t<value>True</value>\n      </setting>\n      <setting name=\"ProxyAddress\" serializeAs=\"String\">\n\t<value />\n      </setting>\n      <setting name=\"ProxyUserName\" serializeAs=\"String\">\n\t<value />\n      </setting>\n      <setting name=\"ProxyDomain\" serializeAs=\"String\">\n\t<value />\n      </setting>\n      <setting name=\"UseProxy\" serializeAs=\"String\">\n\t<value>False</value>\n      </setting>\n      <setting name=\"ProxyByPassOnLocal\" serializeAs=\"String\">\n\t<value>False</value>\n      </setting>\n      <setting name=\"ProxyPort\" serializeAs=\"String\">\n\t<value>0</value>\n      </setting>\n      <setting name=\"RequestTimeout\" serializeAs=\"String\">\n\t<value>30000</value>\n      </setting>\n      <setting name=\"SpeedLimitEnabled\" serializeAs=\"String\">\n\t<value>False</value>\n      </setting>\n      <setting name=\"MaxRate\" serializeAs=\"String\">\n\t<value>0</value>\n      </setting>\n      <setting name=\"ProxyPassword\" serializeAs=\"String\">\n\t<value />\n      </setting>\n      <setting name=\"RegUrl\" serializeAs=\"String\">\n\t<value>http://50.115.121.228/registera99test.php</value>\n      </setting>\n      <setting name=\"RepairUrl\" serializeAs=\"String\">\n\t<value>http://dekaronuprising.net/update9ob2/repair/</value>\n      </setting>\n    </LauncherV2.Core.Settings>\n  </userSettings>\n</configuration>";
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            this.StartupUri = new System.Uri("MainWindow.xaml", System.UriKind.Relative);
        }

        private void InitEmbeddedDllLoading()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                var name = typeof(App).Namespace + ".Resources." + new System.Reflection.AssemblyName(args.Name).Name + ".dll";
                // TODO: FIX THIS FOR CUSTOM THEMES
                if (name.Contains("PresentationFramework"))
                {
                    //MessageBox.Show("There is an error finding the correct theme. \n\nIf you are using a custom theme, please revert to a standard Windows based theme such as Aero / Classic / XP Default \n\nThe launcher will crash while on this theme, Now closing launcher...", "Error::PresentationFramework", MessageBoxButton.OK, MessageBoxImage.Error);
                    //this.Shutdown();
                    name = "PresentationFramework.Classic.resources";
                    string manifestName = "LauncherV2.app.Resources.PresentationFramework.Classic.dll";
                    if (System.Environment.OSVersion.Version.Major < 6)
                    {
                        manifestName = "LauncherV2.app.Resources.PresentationFramework.Classic.XP.dll";
                    }
                    System.IO.Stream s = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(manifestName);
                    System.IO.FileStream fs = System.IO.File.Create("PresentationFramework.Classic.dll", (int)s.Length);
                    byte[] bytes = new byte[s.Length];
                    s.Read(bytes, 0, bytes.Length);
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                }
                //var resourceName = "LauncherV2.app.Resources." +
                //    name + ".dll";

                

                using (var stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
                {
                    Byte[] assemblyData = new Byte[stream.Length];

                    stream.Read(assemblyData, 0, assemblyData.Length);

                    return System.Reflection.Assembly.Load(assemblyData);

                }

            };
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            // hook on error before app really starts
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            base.OnStartup(e);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // TODO: Put Code here to email/upload info on crash through web service
            MessageBox.Show(e.ExceptionObject.ToString());
        }

        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [System.STAThreadAttribute()]
        public static void Main()
        {
            writeSettings();
            var app = new App();
            app.InitEmbeddedDllLoading();
            app.InitializeComponent();
            app.Run();
        }

        public static void writeSettings()
        {
            if (!File.Exists("RisingLauncher.exe.config"))
            {
                File.WriteAllText("RisingLauncher.exe.config", _settings);
            }
        }
    }
}
