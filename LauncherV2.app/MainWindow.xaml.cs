﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using LauncherV2.Core;
using LauncherV2.Core.Common;
using LauncherV2.Core.Concurrency;
using LauncherV2.Core.Http;
using LauncherV2.Core.JSONObjects;
using LauncherV2.Core.Protocol;
using LauncherV2.Core.Repair;
using MahApps.Metro.Controls;
using SevenZip;

namespace LauncherV2.app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public enum ThreadType { VersionCheck, GetNews, Patch, Download, SevenZ, GetGameSettings, WriteGameSettings, Max }
        private enum PatchState { preparing, patching, done }
        private PatchState patcherState = PatchState.preparing;
        private Thread[] threads = new Thread[(int)ThreadType.Max];
        static System.Windows.Forms.Timer refreshTimer = new System.Windows.Forms.Timer();
        private Color lightBlue = new Color() { A = 255, R = 19, G = 121, B = 169 };
        private volatile Downloader primaryDownload = null;
        private volatile Patch currentPatch = null;
        Hashtable patchToDownloadMap = new Hashtable();
        Hashtable downloadToPatchMap = new Hashtable();
        private volatile bool _clearToExtract = true;
        string error = "";
        private double patchId = 0.0;

        private const double Version = 0.7;

        private volatile Queue<Downloader> extractQ = new Queue<Downloader>();

        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            InitApplication();
        }

        public void Info_Click(object sender, RoutedEventArgs e)
        {
            TabControl.SelectedIndex = 5;
        }

        private void VoteBrowser_Initialized(object sender, EventArgs e)
        {
        }

        private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            BtnPlay.IsEnabled = false;
            Thread start = new Thread(new ThreadStart(StartGame));
            start.Name = "Start";
            start.IsBackground = true;
            start.Start();
        }

        private void StartGame()
        {
            Process p = new Process();
            p.StartInfo.FileName = Directory.GetCurrentDirectory()+"/bin/dkrising.exe";
            p.Start();

            Thread.Sleep(2000);
            ThreadSafe.SetControlPropertyThreadSafe(BtnPlay, "IsEnabled", true);
        }

        #region SETUP

        private void InitApplication()
        {
            
            InitializeProtocols();
            UpdateVoteString();
            SetupThreads();
            threads[(int)ThreadType.VersionCheck].Start();
            refreshTimer.Tick += new EventHandler(Tick);
            refreshTimer.Interval = 500;
            refreshTimer.Start();
        }

        void CheckRisingRunning()
        {
            if (isProcessRunning("dkrising"))
            {
                MessageBox.Show("Please close any running instances of Dekaron Rising to update!", "Dekaron Rising running", MessageBoxButton.OK, MessageBoxImage.Error);
                SHUTDOWN();
                return;
            }
        }

        bool isProcessRunning(string process)
        {
            System.Diagnostics.Process[] processes = Process.GetProcessesByName(process);
            foreach (Process proc in Process.GetProcesses())
            {
                if (proc.ProcessName.Contains(process))
                    return true;
            }
            return false;
        }

        /**
         * SetupThreads()
         *  - Create all major threads to be used and add to the threads array
         **/
        private void SetupThreads()
        {
            Thread vc = new Thread(new ThreadStart(VersionCheckWorker));
            vc.Name = "Version Check";
            vc.IsBackground = true;
            threads[(int)ThreadType.VersionCheck] = vc;
            Thread news = new Thread(new ThreadStart(NewsWorker));
            news.Name = "Get News";
            news.IsBackground = true;
            news.SetApartmentState(ApartmentState.STA);
            threads[(int)ThreadType.GetNews] = news;
            Thread patch = new Thread(new ThreadStart(PatchRunner));
            patch.Name = "Patch";
            patch.IsBackground = true;
            threads[(int)ThreadType.Patch] = patch;
            Thread seven = new Thread(new ThreadStart(SevenZCheckWorker));
            seven.Name = "SevenZ";
            seven.IsBackground = true;
            threads[(int)ThreadType.SevenZ] = seven;
            Thread gamesettings = new Thread(new ThreadStart(GetGameSettingsWorker));
            gamesettings.Name = "Game Settings";
            gamesettings.IsBackground = true;
            threads[(int)ThreadType.GetGameSettings] = gamesettings;
        }

        private void SHUTDOWN()
        {
            Dispatcher.BeginInvoke((Action)delegate()
            {
                Application.Current.Shutdown();
            });
        }


        /**
         * VersionOK
         *  - Run after version check is complete, start normal launcher threads
         **/
        private void VersionOK()
        {
            threads[(int)ThreadType.GetNews].Start();
            threads[(int)ThreadType.Patch].Start();
            threads[(int)ThreadType.SevenZ].Start();
            //threads[(int)ThreadType.GetGameSettings].Start();
        }

        private void InitializeProtocols()
        {
            ProtocolProviderFactory.RegisterProtocolHandler("http", typeof(HttpProtocolProvider));
            ProtocolProviderFactory.RegisterProtocolHandler("https", typeof(HttpProtocolProvider));
            ProtocolProviderFactory.RegisterProtocolHandler("ftp", typeof(FtpProtocolProvider));
        }

        #endregion

        #region SevenZCheck

        /**
         * SevenZCheckWorker
         * 
         * Check is 7z.dll exists in the working directory.  If it does not, copy it from the embedded
         * resource 7z.dll and write it to disk
         **/
        private void SevenZCheckWorker()
        {
            if (!File.Exists("7z.dll"))
            {
                Stream s = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("LauncherV2.app.Resources.7z.dll");
                FileStream fs = File.Create("7z.dll", (int)s.Length);
                byte[] bytes = new byte[s.Length];
                s.Read(bytes, 0, bytes.Length);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }

            if (!File.Exists("SevenZipSharp.dll"))
            {
                Stream s = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("LauncherV2.app.Resources.SevenZipSharp.dll");
                FileStream fs = File.Create("SevenZipSharp.dll", (int)s.Length);
                byte[] bytes = new byte[s.Length];
                s.Read(bytes, 0, bytes.Length);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
        }

        #endregion

        #region TIMING

        private void Tick(Object obj, EventArgs e)
        {
            UpdateDownloadProgress();
        }
        #endregion

        #region VERSIONING

        public void VersionCheckWorker()
        {
            string jsonString = Get.GetMessage(LauncherV2.Core.Settings.Default.PatchUrl + "version.json");
            double version = ((LauncherV2.Core.JSONObjects.Version)fastJSON.JSON.Instance.ToObject(jsonString, typeof(LauncherV2.Core.JSONObjects.Version))).version;
            if (version > Version)
            {
                // Launcher is out of date, close and run the launcher updater
                ProcessStartInfo pi = new ProcessStartInfo();
                pi.FileName = Environment.CurrentDirectory + "/LauncherUpdate.exe";
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    pi.Verb = "runas";
                }
                Debug.WriteLine(pi.FileName);
                Process.Start(pi);
                Application.Current.Shutdown();
            }
            else
            {
                VersionOK();
            }
        }

        #endregion

        #region PATCHER

        public void PatchRunner()
        {
            CheckRisingRunning();

            // Get patches
            string jsonString = Get.GetMessage(LauncherV2.Core.Settings.Default.PatchUrl + "patches.json");
            Patches patches =
                (Patches)fastJSON.JSON.Instance.ToObject(jsonString, typeof(Patches));
            Queue<Patch> patchQ = new Queue<Patch>();
            foreach (Patch patch in patches.patches)
            {
                if (patch.id > Settings.Default.CurrentPatch)
                {
                    Settings.Default.CurrentPatch = patch.id;
                    ResourceLocation rl = new ResourceLocation();
                    rl.URL = patch.url;
                    if (patch.authenticate)
                    {
                        rl.Authenticate = true;
                        rl.Login = patch.user;
                        rl.Password = patch.password;
                    }
                    rl.BindProtocolProviderType();
                    if (rl.ProtocolProviderType == null)
                    {
                        MessageBoxResult res = MessageBox.Show("Update Failure::Invalid Protocol\nLauncher will now close. Please try again.\n\n\nWould you like to send an error report to DKRising?", "FATAL ERROR", MessageBoxButton.YesNo, MessageBoxImage.Error);
                        if (res == MessageBoxResult.Yes)
                        {
                            MessageBox.Show("sent");
                        }
                        Debug.WriteLine("Failed Patch: " + fastJSON.JSON.Instance.ToJSON(patch).ToString());
                        SHUTDOWN();
                        return;
                    }

                    Debug.WriteLine("Queing Patch for download: " + fastJSON.JSON.Instance.ToJSON(patch).ToString());
                    Downloader d = new Downloader(rl, null, "patch/" + patch.name + ".rpa", Settings.Default.MaxSegments);
                    patchToDownloadMap[patch] = d;
                    downloadToPatchMap[d] = patch;
                    patchQ.Enqueue(patch);
                }
            }

            Thread extractThread = new Thread(new ThreadStart(ExtractWorker));
            extractThread.Name = "Extract";
            extractThread.IsBackground = true;
            while (patchQ.Count > 0)
            {
                currentPatch = patchQ.Dequeue();
                primaryDownload = (Downloader)patchToDownloadMap[currentPatch];
                primaryDownload.Ending += delegate(Object obj, EventArgs e)
                {
                    extractQ.Enqueue(primaryDownload);
                };
                primaryDownload.Start();
                ChangeState(PatchState.patching);
                primaryDownload.WaitForConclusion();
                if (!extractThread.IsAlive)
                {
                    extractThread.Start();
                }
            }
            SetReady();
            Settings.Default.Save();
        }

        private void ExtractWorker()
        {
            while (threads[(int)ThreadType.SevenZ].IsAlive) { }

            Downloader d = null;
            while (patcherState != PatchState.done)
            {
                while (extractQ.Count > 0)
                {
                    if (_clearToExtract)
                    {
                        if (d != null)
                        {
                            try
                            {
                                File.Delete(d.LocalFile);
                            }
                            catch (Exception e) { }
                        }
                        
                        ThreadSafe.SetControlPropertyThreadSafe(LblExtractStatus, "Visibility", Visibility.Visible);
                        ThreadSafe.SetControlPropertyThreadSafe(PBMicroExtract, "Visibility", Visibility.Visible);
                        _clearToExtract = false;
                        d = extractQ.Dequeue();
                        Patch p = (Patch)downloadToPatchMap[d];
                        patchId = p.id;
                        SevenZip.SevenZipExtractor.SetLibraryPath(@"7z.dll");
                        var extractor = new SevenZipExtractor(d.LocalFile);
                        extractor.Extracting += new EventHandler<ProgressEventArgs>(extractor_Extracting);
                        extractor.ExtractionFinished += new EventHandler<EventArgs>(extractor_ExtractionFinished);
                        extractor.BeginExtractArchive(".\\");
                    }
                }
                Thread.Sleep(500);
            }
        }

        private void extractor_Extracting(object sender, ProgressEventArgs e)
        {
            ThreadSafe.SetControlPropertyThreadSafe(LblExtractStatus, "Content", "Extracting...\t" + e.PercentDone.ToString());
            ThreadSafe.SetControlPropertyThreadSafe(PBMicroExtract, "Value", e.PercentDone);
        }

        private void extractor_ExtractionFinished(object sender, EventArgs e)
        {
            ThreadSafe.SetControlPropertyThreadSafe(LblExtractStatus, "Content", "Extracting...");
            ThreadSafe.SetControlPropertyThreadSafe(PBMicroExtract, "Value", 0);
            ThreadSafe.SetControlPropertyThreadSafe(LblExtractStatus, "Visibility", Visibility.Hidden);
            ThreadSafe.SetControlPropertyThreadSafe(PBMicroExtract, "Visibility", Visibility.Hidden);
            Settings.Default.CurrentPatch = patchId;
            Settings.Default.Save();
            _clearToExtract = true;
        }

        private void ChangeState(PatchState state)
        {
            patcherState = state;
        }

        public void UpdateDownloadProgress()
        {
            switch((int)patcherState)
            {
                case (int)PatchState.preparing:
                    {
                        txtMicroStatus.Text = "preparing";
                    }
                    break;
                case (int)PatchState.patching:
                {
                    txtMicroStatus.Text = "Downloading "+currentPatch.name+"\t Rate: " + String.Format("{0:0.##}", primaryDownload.Rate / 1024.0) + "KB/s  Left: " + TimeSpanFormatter.ToString(primaryDownload.Left)
                        + "  Progress: " + String.Format("{0:0.##}%", primaryDownload.Progress);

                    PBMicro.Value = primaryDownload.Progress;
                }
                break;
                case (int)PatchState.done:
                {
                    txtMicroStatus.Text = "Downloads complete";
                    PBMicro.Value = 100.0;
                }
                break;
            }
        }

        private void DownloadFiles(Queue<LauncherV2.Core.JSONObjects.Patch> q)
        {
            // Get all information for files

        }

        public void SetReady()
        {
            refreshTimer.Stop();
            while (extractQ.Count > 0) { Thread.Sleep(1000); }
            if (patchToDownloadMap.Count > 0)
            {
                try
                {
                    Directory.Delete("patch", true);
                }
                catch (Exception e) { }
            }
            Debug.WriteLine("Client Ready");
            ThreadSafe.SetControlPropertyThreadSafe(txtMicroStatus, "Text", "Client is up to date");
            ThreadSafe.SetControlPropertyThreadSafe(PBMicro, "Value", 100.0);
            ThreadSafe.SetControlPropertyThreadSafe(LblExtractStatus, "Visibility", Visibility.Hidden);
            ThreadSafe.SetControlPropertyThreadSafe(PBMicroExtract, "Visibility", Visibility.Hidden);
            ThreadSafe.SetControlPropertyThreadSafe(BtnPlay, "IsEnabled", true);
        }

        #endregion

        #region REPAIR

        public void Repair_Click(object sender, RoutedEventArgs e)
        {
            if (threads[(int)ThreadType.Patch].IsAlive)
            {
                MessageBox.Show("Can not repair while a patch is in progress!", "Client Repair", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (MessageBox.Show("Would you like to repair your client?\n\nThis process can take anywhere from 3-15 minutes.", "Client Repair?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                BtnPlay.IsEnabled = false;
                Thread repThread = new Thread(new ThreadStart(new Action(() =>
                {
                    Repairer.SetProgressBarControl(PBMicro);
                    Repairer.SetProgressTextControl(txtMicroStatus);
                    Repairer.OnFinished += new Action(() =>
                    {
                        ThreadSafe.SetControlPropertyThreadSafe(BtnPlay, "IsEnabled", true);
                        ThreadSafe.SetControlPropertyThreadSafe(txtMicroStatus, "Text", "Client repair complete!");
                        ThreadSafe.SetControlPropertyThreadSafe(PBMicro, "Value", 100.0);
                    });
                    Repairer.Repair();
                })));
                repThread.Name = "Repair";
                repThread.IsBackground = true;
                repThread.Start();
            }
        }

        #endregion

        #region NEWS

        public void NewsWorker()
        {
            int retryCount = 0;
            LauncherV2.Core.JSONObjects.NewsCollection news = null;
            string jsonString = null;
            do
            {
                jsonString = Get.GetMessage(LauncherV2.Core.Settings.Default.NewsUrl);
                try
                {
                    news = (LauncherV2.Core.JSONObjects.NewsCollection)fastJSON.JSON.Instance.ToObject(jsonString, typeof(LauncherV2.Core.JSONObjects.NewsCollection));

                }
                catch (Exception ex) { }

                retryCount++;
            } while (news == null && retryCount < LauncherV2.Core.Settings.Default.MaxRetries && jsonString != null);

            if (jsonString == null)
            {
                NewsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                { NewsPanel.Children.Add(new TextBlock() { Text = "Error Retrieving News : Invalid URL", FontSize = 18.0f, 
                    Foreground = Brushes.Red, TextAlignment = TextAlignment.Center, Margin = new Thickness(0, 15, 0, 0) }); }));
            }
            else if (news == null)
            {
                 NewsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                { NewsPanel.Children.Add(new TextBlock() { Text = "Error Retrieving News : Invalid Response", FontSize = 18.0f, 
                    Foreground = Brushes.Red, TextAlignment = TextAlignment.Center, Margin = new Thickness(0, 15, 0, 0) }); }));
            }
            else
            {
                Color linkOrange = new Color() { A = 255, R = 255, G = 127, B = 30 };

                foreach (LauncherV2.Core.JSONObjects.NewsObject n in news.news)
                {
                    // Display the date
                    NewsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    { NewsPanel.Children.Add(new TextBlock() { Text = n.date, FontSize = 12.0f, Foreground = Brushes.LightGray, 
                        Padding = new Thickness(5, 10, 0, 0) }); }));
                    // Display title link if there is one
                    if (n.title != String.Empty)
                    {
                        NewsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                        {
                            Run linkText = new Run(n.title);
                            Hyperlink link = new Hyperlink(linkText) { NavigateUri = new Uri(n.titleURL), TextDecorations = null, FontWeight = FontWeights.SemiBold, FontSize = 22.0f, Foreground = new SolidColorBrush(linkOrange) };
                            link.RequestNavigate += Hyperlink_Nav;
                            link.MouseEnter += Hyperlink_OnMouseEnter;
                            link.MouseLeave += Hyperlink_OnMouseLeave;
                            TextBlock tbLink = new TextBlock();
                            tbLink.Inlines.Add(link);
                            NewsPanel.Children.Add(tbLink);
                        }));
                    }
                    // Display Message if there is one
                    if (n.message != String.Empty)
                    {
                        NewsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                        {
                            NewsPanel.Children.Add(new TextBlock()
                            {
                                Text = n.message,
                                FontSize = 16.0f,
                                Padding = new Thickness(15, 2, 0, 0),
                                TextWrapping = TextWrapping.Wrap
                            });
                        }));
                    }
                    // Add a separator
                    NewsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        NewsPanel.Children.Add(new Separator() {Height=1.0f, Margin = new Thickness(0,10,0,0)});
                    }));
                }
            }
            
            ThreadSafe.SetControlPropertyThreadSafe(NewsProgress, "IsActive", false);
        }

        public void Hyperlink_Nav(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo((sender as Hyperlink).NavigateUri.ToString()));
            e.Handled = true;
        }

        // Display the underline on only the MouseEnter event. 
        private void Hyperlink_OnMouseEnter(object sender, EventArgs e)
        {
            (sender as Hyperlink).TextDecorations = TextDecorations.Underline;
        }

        // Remove the underline on the MouseLeave event. 
        private void Hyperlink_OnMouseLeave(object sender, EventArgs e)
        {
            (sender as Hyperlink).TextDecorations = null;
        }

        #endregion

        #region VOTE
        private void BtnVote_Click(object sender, RoutedEventArgs e)
        {
            DateTime next = LauncherV2.Core.Settings.Default.LastVoteTime.AddHours(12.0f);
            if (next < DateTime.Now)
            {
                VoteProgress.IsActive = true;
                Thread voteThread = new Thread(new ThreadStart(VoteThreadWorker));
                voteThread.Name = "Vote";
                voteThread.IsBackground = true;
                voteThread.Start();
            }
        }

        private void VoteThreadWorker()
        {
            try
            {
                string site = LauncherV2.Core.Settings.Default.VoteLink.Split('=')[1];
                NameValueCollection _Params = new NameValueCollection();
                _Params.Add("site", site);
                Post.PostMessage(LauncherV2.Core.Settings.Default.VoteLink, _Params);
                ThreadSafe.SetControlPropertyThreadSafe(TxtVote, "Text", "Success, Thank you for your support!");
                LauncherV2.Core.Settings.Default.LastVoteTime = DateTime.Now;
                LauncherV2.Core.Settings.Default.Save();
                ThreadSafe.SetControlPropertyThreadSafe(VoteProgress, "IsActive", false);
                UpdateVoteString();
            }
            catch (Exception ex) { }
        }

        private void UpdateVoteString()
        {
            if (Settings.Default.LastVoteTime == DateTime.MinValue)
            {
                ThreadSafe.SetControlPropertyThreadSafe(TxtLastVoteTime, "Text", "You have not yet voted");
                ThreadSafe.SetControlPropertyThreadSafe(TxtVoteAvailable, "Text", "Vote now to support the server!");
                ThreadSafe.SetControlPropertyThreadSafe(BtnVote, "IsEnabled", true);
            }
            else
            {
                ThreadSafe.SetControlPropertyThreadSafe(TxtLastVoteTime, "Text", "Last Voted: " 
                    + LauncherV2.Core.Settings.Default.LastVoteTime.ToLongDateString() 
                    + " at " + LauncherV2.Core.Settings.Default.LastVoteTime.ToLongTimeString());

                DateTime next = LauncherV2.Core.Settings.Default.LastVoteTime.AddHours(12.0f);
                if (next > DateTime.Now)
                {
                    ThreadSafe.SetControlPropertyThreadSafe(BtnVote, "IsEnabled", false);
                    ThreadSafe.SetControlPropertyThreadSafe(TxtVoteAvailable, "Text", "You can vote next on: " 
                        + next.ToLongDateString() + " at " + next.ToLongTimeString());
                }
                else
                {
                    ThreadSafe.SetControlPropertyThreadSafe(BtnVote, "IsEnabled", true);
                    ThreadSafe.SetControlPropertyThreadSafe(TxtVoteAvailable, "Text", "You can vote now!");
                }
            }
        }
        #endregion

        #region REGISTER

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            RegProgress.IsActive = true;
            Thread registerThread = new Thread(new ThreadStart(RegisterWorker));
            registerThread.Name = "Register";
            registerThread.IsBackground = true;
            registerThread.Start();
        }

        private bool VerifyInput(string user, string pass, string passConfirm, string email)
        {
            if (user.Length < 4 || user.Length > 12)
            {
                error += "Username must be between 4 and 12 characters\n";
                return false;
            }
            if (pass.Length < 4 || pass.Length > 12)
            {
                error = "Password must be between 4 and 12 characters\n";
                return false;
            }
            if (String.Compare(pass, passConfirm) != 0)
            {
                error = "Passwords do not match\n";
                return false;
            }
            //if (!System.Text.RegularExpressions.Regex.IsMatch(email, @"[a-zA-Z0-9.!#$%&'*+-/=?\^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)* "))
            //{
            //    error = "Invalid email format\n";
            //    return false;
            //}
            //if (!System.Text.RegularExpressions.Regex.IsMatch(user, @"/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/i"))
            //{
            //    error = "Username can only include numbers and letters\n";
            //    return false;
            //}
            //if (!System.Text.RegularExpressions.Regex.IsMatch(pass, @"/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/i"))
            //{
            //    error = "Password cann only include numbers and letters\n";
            //    return false;
            //}
            if(String.Compare(user,pass) == 0)
            {
                error = "Username must difer from password\n";
                return false;
            }
            return true;
        }

        private void ClearInput()
        {
            ThreadSafe.SetControlPropertyThreadSafe(TxtUserName, "Text", "");
            ThreadSafe.SetControlPropertyThreadSafe(TxtEmail, "Text", "");
            ThreadSafe.SetControlPropertyThreadSafe(TxtPassword, "Password", "");
            ThreadSafe.SetControlPropertyThreadSafe(TxtPasswordConfirm, "Password", "");
        }

        private void RegisterWorker()
        {
            string userName = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { userName = TxtUserName.Text; });

            string pass = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { pass = TxtPassword.Password; });

            string passConfirm = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { passConfirm = TxtPasswordConfirm.Password; });

            string email = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { email = TxtEmail.Text; });

            if (VerifyInput(userName, pass, passConfirm, email))
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("accname",userName);
                nvc.Add("accpass1",pass);
                nvc.Add("accpass2",pass);
                nvc.Add("accmail",email);
                byte[] response = Post.PostMessage("http://50.115.121.228/registera99test.php",nvc);
                string message = Encoding.UTF8.GetString(response);
                if(message.Contains("Error")){
                    ThreadSafe.SetControlPropertyThreadSafe(LabelError, "Foreground", Brushes.Red);
                    ThreadSafe.SetControlPropertyThreadSafe(LabelError, "Text", message);
                }
                else 
                {
                    ThreadSafe.SetControlPropertyThreadSafe(LabelError, "Text", "Success, Account created!");
                    ThreadSafe.SetControlPropertyThreadSafe(LabelError, "Foreground", Brushes.Chartreuse);
                    ClearInput();
                }
            } else {
                ThreadSafe.SetControlPropertyThreadSafe(LabelError, "Foreground", Brushes.Yellow);
                ThreadSafe.SetControlPropertyThreadSafe(LabelError, "Text", error);
            }
            ThreadSafe.SetControlPropertyThreadSafe(RegProgress, "IsActive", false);
        }

        #endregion

        #region SETTINGS

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            TabControl.SelectedIndex = 4;
        }

        private void GetGameSettingsWorker()
        {
            // TEMP DISABLED
            return;

            try
            {
                FileStream optionsFile = File.Open("bin/option.txt", FileMode.Open);
                StreamReader reader = new StreamReader(optionsFile);
                do
                {
                    String[] line = reader.ReadLine().Split('\t');
                    String name = line[0];
                    String value = line[line.Length-1];
                    GameSettingsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                        {
                            //StackPanel sp = new StackPanel() {Orientation=Orientation.Horizontal, HorizontalAlignment=HorizontalAlignment.Center};
                            Grid grid = new Grid() { Margin = new Thickness(10,0,0,10) };
                            ColumnDefinition col1 = new ColumnDefinition() { Width = new GridLength(250)};
                            ColumnDefinition col2 = new ColumnDefinition() { Width = new GridLength(1,GridUnitType.Star)};
                            grid.ColumnDefinitions.Add(col1);
                            grid.ColumnDefinitions.Add(col2);

                            TextBlock label = CreateTextBlockLabel(name);
                            Grid.SetColumn(label, 0);
                            grid.Children.Add(label);

                            TextBox property = CreateStringInput(value);
                            Grid.SetColumn(property, 1);
                            grid.Children.Add(property);
                            GameSettingsPanel.Children.Add(grid);
                        }));


                } while(reader.Peek() != -1);

            }
            catch (Exception e) {
                GameSettingsPanel.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    GameSettingsPanel.Children.Add(new TextBlock()
                    {
                        Text = "Error Retrieving Game Options",
                        FontSize = 18.0f,
                        Foreground = Brushes.Red,
                        TextAlignment = TextAlignment.Center,
                        Margin = new Thickness(0, 15, 0, 0)
                    });
                }));
            }

            ThreadSafe.SetControlPropertyThreadSafe(GameSettingsProgress, "IsActive", false);
        }

        #endregion

#region CONTROL BUILDERS

        private TextBlock CreateTextBlockLabel(string value)
        {
            return new TextBlock() { Text = value, FontSize = 18.0, TextAlignment=TextAlignment.Right, Margin = new Thickness(0,0,25,0)};
        }

        private TextBox CreateStringInput(string value)
        {
            return new TextBox() { Text = value, MinWidth = 150.0, Margin = new Thickness(10,0,0,0)};
        }

        private CheckBox CreateOnOffSwitch(bool setting)
        {
            return new CheckBox() { IsChecked = setting, Style = Resources["SwitchStyle"] as Style };
        }

#endregion

        private void btnChangePass_Click(object sender, RoutedEventArgs e)
        {
            cpProgress.IsActive = true;
            Thread cpThread = new Thread(new ThreadStart(ChangePassWorker));
            cpThread.Name = "Change Pass";
            cpThread.IsBackground = true;
            cpThread.Start();
        }

        private bool cpVerifyInput(string user, string oldpass, string pass, string passConfirm)
        {
            if (user.Length < 4 || user.Length > 12)
            {
                error += "Username must be between 4 and 12 characters\n";
                return false;
            }
            if (oldpass.Length < 4 || pass.Length > 12)
            {
                error = "Password must be between 4 and 12 characters\n";
                return false;
            }
            if (pass.Length < 4 || pass.Length > 12)
            {
                error = "Password must be between 4 and 12 characters\n";
                return false;
            }
            if (String.Compare(pass, passConfirm) != 0)
            {
                error = "Passwords do not match\n";
                return false;
            }
            if (String.Compare(user, pass) == 0)
            {
                error = "Username must difer from password\n";
                return false;
            }
            return true;
        }

        private void cpClearInput()
        {
            ThreadSafe.SetControlPropertyThreadSafe(tbcpUserName, "Text", "");
            ThreadSafe.SetControlPropertyThreadSafe(tbcpNewPass, "Password", "");
            ThreadSafe.SetControlPropertyThreadSafe(tbcpConfirmPass, "Password", "");
            ThreadSafe.SetControlPropertyThreadSafe(tbCPOldPass, "Password", "");
        }

        private void ChangePassWorker()
        {
            string userName = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { userName = tbcpUserName.Text; });

            string oldpass = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { oldpass = tbCPOldPass.Password; });

            string pass = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { pass = tbcpNewPass.Password; });

            string passConfirm = "";
            System.Windows.Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                (ThreadStart)delegate { passConfirm = tbcpConfirmPass.Password; });

            if (cpVerifyInput(userName, oldpass, pass, passConfirm))
            {
                using (MD5 md5 = MD5.Create())
                {
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("user_id", userName);
                    nvc.Add("currentpass", GetMd5Hash(md5,oldpass));
                    nvc.Add("newpass", GetMd5Hash(md5, pass));
                    byte[] response = Post.PostMessage("http://72.8.157.174:8889/api/accounts/changepass", nvc);
                    string message = Encoding.UTF8.GetString(response);
                    if (!message.Contains("Success"))
                    {
                        ThreadSafe.SetControlPropertyThreadSafe(cpError, "Foreground", Brushes.Red);
                        ThreadSafe.SetControlPropertyThreadSafe(cpError, "Text", "ERROR : " + message);
                    }
                    else
                    {
                        ThreadSafe.SetControlPropertyThreadSafe(cpError, "Text", "Success, Pass changed!");
                        ThreadSafe.SetControlPropertyThreadSafe(cpError, "Foreground", Brushes.Chartreuse);
                        ClearInput();
                    }
                }
            }
            else
            {
                ThreadSafe.SetControlPropertyThreadSafe(cpError, "Foreground", Brushes.Yellow);
                ThreadSafe.SetControlPropertyThreadSafe(cpError, "Text", "ERROR : " + error);
            }
            ThreadSafe.SetControlPropertyThreadSafe(cpProgress, "IsActive", false);
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}