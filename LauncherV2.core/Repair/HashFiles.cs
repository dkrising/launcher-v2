﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.Repair
{
    [Serializable]
    public class HashFile
    {
        public string name { get; set; }
        public string url { get; set; }
        public string path { get; set; }
        public DateTime modified { get; set; }
        public int size { get; set; }
        public string hash { get; set; }
    }

    [Serializable]
    public class HashFiles
    {
        public List<HashFile> files { get; set; }
        public HashFiles()
        {
            files = new List<HashFile>();
        }
    }
}
