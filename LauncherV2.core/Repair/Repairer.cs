﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LauncherV2.Core.Hash;
using System.Diagnostics;
using System.Threading;
using LauncherV2.Core.JSONObjects;
using fastBinaryJSON;
using System.Windows.Controls;
using LauncherV2.Core.Common;
using LauncherV2.Core.Concurrency;

namespace LauncherV2.Core.Repair
{
    public static class Repairer
    {
        private static string currentDirectory = Directory.GetCurrentDirectory();
        private static Files files = new Files();
        private static List<Hash> hashes = new List<Hash>();
        private static int threadsRemaining;
        private static string baseUrl;
        private static Dictionary<string, int> pathToIndex = new Dictionary<string, int>();
        private static bool checkComplete = false;

        private static ProgressBar pBar = null;
        private static TextBlock pText = null;
        private static Downloader downloader = null;
        public static Action OnFinished = null;

        public static int fileCount = 0;
        public static int filesRemaining = 0;


        public static void SetProgressBarControl(ProgressBar bar)
        {
            pBar = bar;
        }
        public static void SetProgressTextControl(TextBlock tb)
        {
            pText = tb;
        }

        private static void Reset()
        {
            files.files.Clear();
            hashes.Clear();
            pathToIndex.Clear();
            baseUrl = Settings.Default.RepairUrl;
            fileCount = 0;
            filesRemaining = 0;
            checkComplete = false;
            if (baseUrl[baseUrl.Length - 1] != '/')
            {
                baseUrl += '/';
            }
        }
        public static void Repair(Action onFinished = null)
        {
            if (pBar == null || pText == null)
                return;
            Reset();
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            try
            {
                Directory.Delete("repair", true);
            }
            catch (Exception e) { }

            // Download index in background
            ResourceLocation rl = new ResourceLocation();
            rl.URL = baseUrl + "index.bjson";
            rl.BindProtocolProviderType();
            downloader = new Downloader(rl, null, "repair/index.bjson", Settings.Default.MaxSegments);
            downloader.Start();

            // Start parsing directory
            parseDirectory(currentDirectory);

            // Hash files
            HashFiles(Directory.GetCurrentDirectory());

            // Compaire files & repair
            CompareAndRepair();
            float val = (timer.ElapsedMilliseconds/1000.0f);
            timer.Stop();
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repair Complete");
            ThreadSafe.SetControlPropertyThreadSafe(pBar, "Value", 100.0);
            if (onFinished != null)
            {
                onFinished();
            }
        }



        public static void CreateIndex(string baseDirectory, Action onFinished)
        {
            Reset();
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            parseDirectory(baseDirectory);
            HashFiles(baseDirectory);
            float hashTime = (timer.ElapsedMilliseconds / 1000.0f);
            WriteDirectory("index.bjson");
            float total = (timer.ElapsedMilliseconds / 1000.0f);
            float writeTime = total - hashTime;
            timer.Stop();
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Index Creation Complete");
            ThreadSafe.SetControlPropertyThreadSafe(pBar, "Value", 100.0);
            onFinished();
        }

        private static void parseDirectory(string dir)
        {
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Parsing Directory...");
            var fcounter = new FindFile(dir, "*", true, true, true);
            fcounter.RaiseOnAccessDenied = false;

            fcounter.FileFound +=
                (o, e) =>
                {
                    if (!e.IsDirectory)
                    {
                        FileObject file = new FileObject();
                        file.path = e.FullPath.Remove(0, dir.Length+1);
                        file.name = e.Name;
                        file.url = baseUrl +"client/"+ file.path.Replace('\\','/');
                        pathToIndex.Add(file.path, files.files.Count);
                        files.files.Add(file);
                        fileCount++;
                        ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Parsing Directory...\t" + fileCount + " files found");
                    }
                };
            fcounter.Find();
        }

        private static void HashFiles(string baseDirectory)
        {
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Hashing files...\tReticulating splines");
            ManualResetEvent done = new ManualResetEvent(false);
            threadsRemaining = files.files.Count;
            for (int i = 0; i < files.files.Count; i++)
            {
                Hash h = new Hash(files.files[i], baseDirectory, i, done);
                hashes.Add(h);
                ThreadPool.QueueUserWorkItem(h.ThreadPoolCallback, i);
            }

            done.WaitOne();

            foreach (Hash h in hashes)
            {
                files.files[h.index].hash = h.file.hash;
            }
        }

        private static void WriteDirectory(string fileName)
        {
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Writing index to disk...");
            byte[] json = fastBinaryJSON.BJSON.Instance.ToBJSON(files);
            File.WriteAllBytes(fileName, json);
        }

        private static void CompareAndRepair()
        {
            Thread timer = new Thread(new ThreadStart(new Action(() =>
            {
                while (downloader.IsWorking())
                {
                    UpdateDownloadProgress();
                    Thread.Sleep(500);
                }
            })));
            timer.Name = "TIMER";
            timer.IsBackground = true;
            timer.Start();
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Waiting on index download...");
            downloader.WaitForConclusion();

            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Checking local files against index...");
            Queue<Downloader> filesToFix = new Queue<Downloader>();

            byte[] index = File.ReadAllBytes("repair/index.bjson");
            Files indexFiles = fastBinaryJSON.BJSON.Instance.ToObject<Files>(index);
            int count = 0;
            fileCount = threadsRemaining = 0;
            for (int i = 0; i < indexFiles.files.Count; i++)
            {
                int value = -1;
                if (pathToIndex.TryGetValue(indexFiles.files[i].path,out value))
                {
                    FileObject localFile = files.files[value];
                    if (!indexFiles.files[i].hash.SequenceEqual(localFile.hash))
                    {
                        Interlocked.Increment(ref threadsRemaining);
                        Interlocked.Increment(ref fileCount);
                        //not equal
                        ResourceLocation rl = new ResourceLocation();
                        rl.URL =indexFiles.files[i].url;
                        rl.BindProtocolProviderType();
                        Downloader d = new Downloader(rl, null, indexFiles.files[i].path, Settings.Default.MaxSegments);
                        d.Ending += FileDownloadComplete;
                        d.Start();
                    }
                }
                else
                {
                    Interlocked.Increment(ref threadsRemaining);
                    Interlocked.Increment(ref fileCount);
                    //not found
                    ResourceLocation rl = new ResourceLocation();
                    rl.URL = indexFiles.files[i].url;
                    rl.BindProtocolProviderType();
                    Downloader d = new Downloader(rl, null, indexFiles.files[i].path, Settings.Default.MaxSegments);
                    d.Ending += FileDownloadComplete;
                    d.Start();
                }
                count++;
                float percent = ((float)i / (float)indexFiles.files.Count)*100.0f;
                ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Reticulating splines ...\t"+percent+"%\t"+i+"/"+indexFiles.files.Count+" files\t"+filesToFix.Count+" files failed hash check");
                ThreadSafe.SetControlPropertyThreadSafe(pBar, "Value", percent);
            }
        }

        private static void Tick(Object obj, EventArgs e)
        {
            UpdateDownloadProgress();
        }

        private static void UpdateDownloadProgress()
        {
            ThreadSafe.SetControlPropertyThreadSafe(pText, "Text", "Repairing Client :: Downloading Index...\t Rate: " + String.Format("{0:0.##}", downloader.Rate / 1024.0) + "KB/s  Left: " + TimeSpanFormatter.ToString(downloader.Left)
                        + "  Progress: " + String.Format("{0:0.##}%", downloader.Progress));

            ThreadSafe.SetControlPropertyThreadSafe(pBar, "Value", downloader.Progress);
        }

        private static void FileDownloadComplete(object o, EventArgs e)
        {

            if (Interlocked.Decrement(ref Repairer.threadsRemaining) == 0)
            {
                RepairComplete();
            }
            else if (checkComplete)
            {
                float percent = 100.0f - ((float)Repairer.threadsRemaining / (float)Repairer.fileCount) * 100.0f;
                ThreadSafe.SetControlPropertyThreadSafe(Repairer.pText, "Text", "Repairing Client :: Downloading files...\t" + percent + "%\t" + Repairer.threadsRemaining + "/" + Repairer.fileCount + " remaining");
                ThreadSafe.SetControlPropertyThreadSafe(Repairer.pBar, "Value", percent);
            }
        }

        private static void RepairComplete()
        {
            if (OnFinished != null)
            {
                OnFinished();
            }
        }

        public class Hash
        {
            string baseDirectory;
            ManualResetEvent done;
            public int index { get; private set; }
            public FileObject file {get; private set; }

            public Hash(FileObject _file, string dir, int ind, ManualResetEvent doneEvent)
            {
                file = _file;
                done = doneEvent;
                index = ind;
                baseDirectory = dir;
            }

            public void ThreadPoolCallback(Object threadContext)
            {
                try
                {
                    HashFile(file.path);
                }
                finally
                {
                    if (Interlocked.Decrement(ref Repairer.threadsRemaining) == 0)
                    {
                        done.Set();
                    }
                    else
                    {
                        float percent = 100.0f - ((float)Repairer.threadsRemaining / (float)Repairer.fileCount) * 100.0f;
                        ThreadSafe.SetControlPropertyThreadSafe(Repairer.pText, "Text", "Repairing Client :: Hashing files...\t"+percent+"%\t" + Repairer.threadsRemaining + "/" + Repairer.fileCount + " remaining");
                        ThreadSafe.SetControlPropertyThreadSafe(Repairer.pBar, "Value", percent);
                    }
                }
            }

            public void HashFile(string path)
            {
                Murmur3 murmur = new Murmur3();
                path = baseDirectory +"/"+ path;
                file.hash = murmur.ComputeHash(File.ReadAllBytes(path));
            }
        }
    }

    
}
