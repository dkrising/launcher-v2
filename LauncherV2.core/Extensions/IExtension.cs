﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.Extensions
{
    public interface IExtension
    {
        string Name { get; }

        IUIExtension UIExtension { get; }
    }
}
