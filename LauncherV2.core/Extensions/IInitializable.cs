﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.Extensions
{
    public interface IInitializable
    {
        void Init();
    }
}
