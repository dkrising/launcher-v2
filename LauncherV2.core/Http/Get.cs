﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;

namespace LauncherV2.Core.Http
{
    public static class Get
    {
        public static string GetMessage(string uri, NameValueCollection pairs = null)
        {
            if (pairs != null)
            {
                uri += "?";
                foreach (KeyValuePair<String, String> pair in pairs)
                {
                    uri += "&" + pair.Key + "=" + pair.Value;
                }
            }
            try
            {

               HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
               req.Method = "GET";
               req.Timeout = 30000;
               //req.Proxy = new System.Net.WebProxy(ProxyString, true);
               HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
               if (resp.ContentLength <= 0)
                   return null;
               System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
               string s = sr.ReadToEnd().Trim();
               return s;
            }
            catch (WebException ex)
            {
                return null;
            }
        }
    }
}
