﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.JSONObjects
{
    [Serializable]
    public class Version
    {
        public double version { get; set; }
    }
}
