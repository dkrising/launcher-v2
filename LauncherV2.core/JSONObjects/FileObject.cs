﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.JSONObjects
{
    [Serializable]
    public class FileObject
    {
        public string name { get; set; }
        public string path { get; set; }
        public string url { get; set; }
        public byte[] hash { get; set; }
    }

    [Serializable]
    public class Files
    {
        public List<FileObject> files { get; set; }
        public Files()
        {
            files = new List<FileObject>();
        }
    }
}
