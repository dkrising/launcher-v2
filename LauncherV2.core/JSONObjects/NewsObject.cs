﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.JSONObjects
{
    [Serializable]
    public class NewsObject
    {
        public string date { get; set; }
        public string title { get; set; }
        public string titleURL { get; set; }
        public string message { get; set; }
    }
    [Serializable]
    public class NewsCollection
    {
        public List<NewsObject> news { get; set; }
        public NewsCollection()
        {
            news = new List<NewsObject>();
        }
    }
}
