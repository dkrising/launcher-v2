﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core.JSONObjects
{
    [Serializable]
    public class Patches
    {
        public List<Patch> patches { get; set; }
    }

    [Serializable]
    public class Patch
    {
        public double id { get; set; }
        public string name { get; set; }
        public string date { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string localPath { get; set; }
        public bool authenticate { get; set; }
        public string user { get; set; }
        public string password { get; set; }

        public Patch()
        {
            authenticate = false;
        }
    }
}
