﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace LauncherV2.Core.SpeedLimit
{
    public interface ISpeedLimitParameters
    {
        bool Enabled { get; set; }

        double MaxRate { get; set; }

        event PropertyChangedEventHandler ParameterChanged;
    }
}
