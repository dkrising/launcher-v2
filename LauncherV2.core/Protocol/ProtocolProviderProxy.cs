﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LauncherV2.Core.SpeedLimit;

namespace LauncherV2.Core.Protocol
{
    public class ProtocolProviderProxy : IProtocolProvider
    {
        private IProtocolProvider proxy;
        private SpeedLimitExt speedLimit;

        #region IProtocolProvider Members

        public void Initialize(Downloader downloader)
        {
            proxy.Initialize(downloader);
        }

        public System.IO.Stream CreateStream(ResourceLocation rl, long initialPosition, long endPosition)
        {
            return new LimitedRateStreamProxy(proxy.CreateStream(rl, initialPosition, endPosition), speedLimit);
        }

        public RemoteFileInfo GetFileInfo(ResourceLocation rl, out System.IO.Stream stream)
        {
            RemoteFileInfo result = proxy.GetFileInfo(rl, out stream);

            if (stream != null)
            {
                stream = new LimitedRateStreamProxy(stream, speedLimit);
            }

            return result;
        }

        #endregion

        public ProtocolProviderProxy(IProtocolProvider proxy, SpeedLimitExt speedLimit)
        {
            this.proxy = proxy;
            this.speedLimit = speedLimit;
        }
    }
}
