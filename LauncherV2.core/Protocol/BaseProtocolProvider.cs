﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace LauncherV2.Core.Protocol
{
    public class BaseProtocolProvider
    {
        static BaseProtocolProvider()
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
        }

        protected WebRequest GetRequest(ResourceLocation location)
        {
            WebRequest request = WebRequest.Create(location.URL);
            request.Timeout = Settings.Default.RequestTimeout;
            SetProxy(request);
            return request;
        }

        protected void SetProxy(WebRequest request)
        {
            if (Settings.Default.UseProxy)
            {
                WebProxy proxy = new WebProxy(Settings.Default.ProxyAddress, Settings.Default.ProxyPort);
                proxy.BypassProxyOnLocal = Settings.Default.ProxyByPassOnLocal;
                request.Proxy = proxy;

                if (!String.IsNullOrEmpty(Settings.Default.ProxyUserName))
                {
                    request.Proxy.Credentials = new NetworkCredential(
                        Settings.Default.ProxyUserName,
                        Settings.Default.ProxyPassword,
                        Settings.Default.ProxyDomain);
                }
            }
        }
    }
}
