﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LauncherV2.Core
{
    [Serializable]
    public class RemoteFileInfo
    {
        public bool AcceptRanges { get; set; }
        public long FileSize { get; set; }
        public string MimeType { get; set; }

        private DateTime lastModified = DateTime.MinValue;
        public DateTime LastModified
        {
            get { return lastModified; }
            set { lastModified = value; }
        }
    }
}
