﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Windows.Threading;
using System.Threading;

namespace LauncherV2.Core.Concurrency
{
    public static class ThreadSafe
    {
        public static void SetControlPropertyThreadSafe(System.Windows.Controls.Control control, string propertyName, object propertyValue)
        {
            control.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => { control.GetType().InvokeMember(propertyName, BindingFlags.SetProperty, null, control, new object[] { propertyValue }); }));
        }

        public static void SetControlPropertyThreadSafe(System.Windows.FrameworkElement control, string propertyName, object propertyValue)
        {
            control.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => { control.GetType().InvokeMember(propertyName, BindingFlags.SetProperty, null, control, new object[] { propertyValue }); }));
        }
    }
}
